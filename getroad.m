function thresh = getroad(img_raw)

img_smooth = imgaussfilt(mat2gray(img_raw),1);

[M,N,C] = size(img_smooth); 
img = img_smooth(1:10:M,1:10:N,:);

my_display(img,'orig.png');


noOfNeighbours=200;

sigma_S = 4;
sigma_I = 0.1;

max_iter = 20;

res= myMeanShiftSegmentation(img,1,sigma_I,sigma_S,max_iter,noOfNeighbours);
my_display(res(:,:,1:3),'Segemnted imgae');

res1 = res(:,:,1:3);
res1(:,:,1) = res1(:,:,1) - 0.60;
res1(:,:,2) = res1(:,:,2) - 0.58;
res1(:,:,3) = res1(:,:,3) - 0.53;

d = res1(:,:,1).*res1(:,:,1) + res1(:,:,2).*res1(:,:,2)+res1(:,:,3).*res1(:,:,3);
q = d;
for i = 1:M/10
    for j = 1:N/10
        if d(i,j)<0.005
            q(i,j) = 1;
        else
            q(i,j) = 0;
        end
    end
end
my_display(q,'road');
thresh = double(zeros(M,N));
for i = 1:M
    for j = 1:N
        thresh(i,j) = q(ceil(i/10),ceil(j/10)); 
        if(thresh(i,j)==1)
            thresh(i,max(1,j-ceil(N/15)):min(N,j+ceil(N/15))) = thresh(i,j);
        end
    end
end
my_display(thresh,'road');