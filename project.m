close all;
im = imread('sample2.png');

% im= imguidedfilter(im);
% im = imgaussfilt(im,3);

% imshow(im);

I=rgb2gray(im);
[M N] = size(I);
Hf = double(zeros(M,N));
Hf(ceil(M/3):M,:) = 1; 
 road = getroad(im);
ed1 = edge(I,'canny',[0.15 0.25]);

imshow(ed1);figure();
imshow(thresh);figure();
ed = ed1.*(road).*Hf;
imshow(ed);


[H,T,R]=hough(ed,'RhoResolution',0.5,'Theta',-80:0.5:80.5);

P = houghpeaks(H,15,'threshold',ceil(0.3*max(H(:))));

lines = houghlines(ed,T,R,P,'FillGap',300,'MinLength',500);

rI = [0.6 0.58 0.53];


figure, imshow(I), hold on
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
 
   % Plot beginnings and ends of lines
   plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
   plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');

   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
end

% highlight the longest line segment
% plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','red');
im = mat2gray(im);
obj = zeros(M,N);
tic;
for i = 1:M
    for j = 1:N
        if ((im(i,j,1)-rI(1))^2+(im(i,j,2)-rI(2))^2+(im(i,j,3)-rI(3))^2>0.03) && road(i,j)*Hf(i,j)==1
            obj(i,j) = 1;
        end
        
        
    end
end
toc;
my_display(obj,'objects');

% imshow(lines);